__HERMES_HOME=${HOME}/.hermes
__HERMES_LINKDIR="${__HERMES_HOME}/links"
__HERMES_DEBUG=
__HERMES_COMMANDS=(add list remove update go)

HERMES_USE_PUSHD=0

function __hermes_install () {
    if [ -d $__HERMES_HOME ]; then
        mkdir -p $__HERMES_LINKS
	echo "hermes.sh - first time loading detected, created home directory"
    fi
}

function __hermes_complete_command () {
    local cur
    cur=${COMP_WORDS[COMP_CWORD]}
    local words
    words="add list remove update go"
    if [[ $COMP_CWORD -eq 1 ]]; then
        COMPREPLY=($( compgen -W "$words" -- "$cur" ));
    fi
    return 0
}

function __hermes_complete_go () {
    local cur
    cur=${COMP_WORDS[COMP_CWORD]}
    local words
    words=`ls ${__HERMES_LINKDIR}`
    if [[ $COMP_CWORD -eq 2 ]]; then
        COMPREPLY=($( compgen -W "$words" -- "$cur" ))
    fi
    return 0
}

function __hermes_init () {
    if [ ! -d ${__HERMES_LINKDIR} ]; then
        mkdir -p ${__HERMES_LINKDIR}
    else
        [ ! -z ${__HERMES_DEBUG} ] && echo "${__HERMES_LINKDIR} already exists"
    fi
}

function __hermes_readlink () {
    if [[ `uname` == "Darwin" ]]; then
        stat -F $1 | awk '{print $NF}'
    else
        readlink -f $1
    fi
}

function __hermes_help () {
    echo "may Hermes guide you to the correct path"
    echo "usage:"
    echo "    hermes list            : lists all paths that Hermes can guide you to"
    echo "    hermes add     <label> : adds the current directory to Hermes' knowledge port, under the mnemonic <label>"
    echo "    hermes remove  <label> : forces Hermes to forget the path to <label>"
    echo "    hermes update  <label> : forces Hermes to redirect <label> to the current directory"
    echo "    hermes go      <label> : instructs Hermes to guide you to <label>"
}

function __hermes_add () {
    if [ -z $1 ]; then
        echo "error: please provide a mnemonic for Hermes to guide you here (hermes add <label>)"
        return 1
    fi

    local new_path=${__HERMES_LINKDIR}/$1
    if [ -L $new_path ]; then
        local where_to=`__hermes_readlink $new_path`
        echo "$new_path already exists ($where_to); use 'hermes update' if you would like to update it"
    else
        echo "created a path to $1 (${PWD})"
        ln -s "${PWD}" "${new_path}"
    fi
}

function __hermes_list () {
    ls -al $__HERMES_LINKDIR
}

function __hermes_cd () {
    if [[ $HERMES_USE_PUSHD -eq 0 ]]; then
        cd $1
    else
        pushd $1
    fi
}

function __hermes_go () {
    if [ -z $1 ]; then
        echo "error: please provide a label for Hermes to guide you there (hermes go <label>)"
        return 1
    fi

    local dest=${__HERMES_LINKDIR}/$1
    local dest_path=`__hermes_readlink $dest`
    if [ ! -L $dest ]; then
        echo "error: path to $1 ($dest_path) does not exist"
    else
        __hermes_cd $dest_path
    fi
}

function __hermes_remove () {
    if [ -z $1 ]; then
        echo "error: please provide a label for Hermes to forget a path (hermes remove <label>)"
        return 1
    fi

    local dest=${__HERMES_LINKDIR}/$1
    if [ ! -L $dest ]; then
        echo "error: path to $1 does not exist"
    else
        rm $dest
    fi
}

function __hermes_update () {
    __hermes_remove $1 && __hermes_add $1
}

function __hermes_main () {
    __hermes_init

    local command_token=$1
    if [ -z "$command_token" ]; then
        echo "please give Hermes a command"
        __hermes_help
        return 1
    fi

    local command_found=0
    for key in "${__HERMES_COMMANDS[@]}"; do
        if [[ "$key" == "$command_token" ]]; then
            command_found=1
        fi
    done

    if [[ $command_found -eq 0 ]]; then
        echo "$command_token: command not found"
        __hermes_help
        return 1
    fi

    [[ "$command_token" == "add" ]]     && __hermes_add $2
    [[ "$command_token" == "list" ]]    && __hermes_list
    [[ "$command_token" == "remove" ]]  && __hermes_remove $2
    [[ "$command_token" == "update" ]]  && __hermes_update $2
    [[ "$command_token" == "go" ]]      && __hermes_go $2

    return 0
}

alias hermes="__hermes_main"

if [[ `uname` == "Darwin" ]]; then
    complete -F __hermes_complete_command hermes
    complete -F __hermes_complete_go hermes go
    complete -F __hermes_complete_go hermes update
    complete -F __hermes_complete_go hermes remove
else
    complete -o default -F __hermes_complete_command hermes
    complete -o default -F __hermes_complete_go hermes go
    complete -o default -F __hermes_complete_go hermes update
    complete -o default -F __hermes_complete_go hermes remove
fi
